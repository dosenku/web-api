<?php

$params = require(__DIR__ . '/params.php');

$baseUrl = str_replace('/web', '', (new \yii\web\Request)->getBaseUrl());


$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [
                
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['activity', 'place'],
                    'pluralize' => false,
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['user'],
                    'pluralize' => false,
                    'extraPatterns' => [
                        'POST search' => 'search',
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['dosen'],
                    'pluralize' => false,
                    'extraPatterns' => [
                        'POST {id}' => 'update',
                        'GET activity/{id}' => 'activity',
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['setting'],
                    'pluralize' => false,
                    'tokens' => [
                        '{id}' => '<id:[\w\-/]+>'
                    ]
                ],
				
				['pattern' => '<controller:[\w\-/]+>/<action:[\w\-/]+>/<id:\d+>', 'route' => '<controller>/<action>/'],
                ['pattern' => '<controller:[\w\-/]+>/<action:[\w\-/]+>', 'route' => '<controller>/<action>'],
                ['pattern' => '<controller:[\w\-/]+>', 'route' => '<controller>/'],

                
            ],
        ],

        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'NO1aHnDV942h6QfsmBbqlsCJg4J4-vcH',

            'baseUrl' => $baseUrl,

            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\UserLogin',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
