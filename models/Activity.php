<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "activity".
 *
 * @property integer $id
 * @property integer $dosen_id
 * @property string $time
 * @property double $longitude
 * @property double $latitude
 * @property integer $place_id
 */
class Activity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dosen_id', 'time', 'longitude', 'latitude'], 'required'],
            [['dosen_id', 'place_id'], 'integer'],
            [['time'], 'safe'],
            [['longitude', 'latitude'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dosen_id' => 'Dosen ID',
            'time' => 'Time',
            'longitude' => 'Longitude',
            'latitude' => 'Latitude',
            'place_id' => 'Place ID',
        ];
    }
}
