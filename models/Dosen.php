<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dosen".
 *
 * @property integer $user_id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $photo
 * @property integer $update_interval
 * @property integer $is_deleted
 */
class Dosen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dosen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'name'], 'required'],
            [['user_id', 'update_interval', 'is_deleted'], 'integer'],
            [['name', 'phone', 'email', 'photo'], 'string', 'max' => 255],
            [['user_id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'name' => 'Name',
            'phone' => 'Phone',
            'email' => 'Email',
            'photo' => 'Photo',
            'update_interval' => 'Update Interval',
            'is_deleted' => 'Is Deleted',
        ];
    }

    public function fields()
    {
        $fields = parent::fields();
        $fields = $fields + [
            'lastSeen' => function($model) {
                $act = Activity::find()->where(['dosen_id' => $model->user_id])->orderBy(['id' => SORT_DESC])->one();
                $res = -1;
                if ($act) {
                    if (time() - strtotime($act->time) <= 2*60) {
                        $res = 0;
                    } else {
                        $res = (int)((time() - strtotime($act->time)) / 60);
                    }
                }

                return $res;
            },
            'place' => function($model) {
                $act = Activity::find()->where(['and', ['dosen_id' => $model->user_id], ['is not', 'place_id', null]])->orderBy(['id' => SORT_DESC])->one();
                $res = "";
                if ($act) {
                    $res = Place::findOne($act->place_id)->name . " (at ".date('H:i', strtotime($act->time)).")";
                }

                return $res;
            },

        ];
        return $fields;
    }
}
