<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;

class PlaceController extends ActiveController {
    public $modelClass = "app\models\Place";

    public function actions()
    {
        $actions = parent::actions();

        //hapus agar bisa dioverride
        unset($actions['delete'], $actions['view']);

        // customize the data provider preparation with the "prepareDataProvider()" method
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function prepareDataProvider()
    {
        // prepare and return a data provider for the "index" action
        $modelClass = $this->modelClass;

        $query = $modelClass::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->andFilterWhere(['!=', 'is_deleted', 1]);

        return $dataProvider;
    }

    public function actionView() {
        $id = Yii::$app->request->get('id');
        $modelClass = $this->modelClass;

        if ($model = $modelClass::find()->where(['and', ['id' => $id], ['!=', 'is_deleted', '1']])->one()) {
            return $model;
        } else {
            throw new \yii\web\NotFoundHttpException('404 Data not Found');
        }
    }

    public function actionDelete() {
        $id = Yii::$app->request->get('id');
        $modelClass = $this->modelClass;

        $model = $modelClass::findOne(['id' => $id]);
        if (!$model) {
            throw new \yii\web\NotFoundHttpException('404 Data not Found');
        }

        $model->is_deleted = 1;
        $model->save();
    }

}
