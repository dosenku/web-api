<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\ServerErrorHttpException;

class UserController extends ActiveController {
    public $modelClass = "app\models\User";

    public function actions()
    {
        $actions = parent::actions();

        //hapus agar bisa dioverride
        unset($actions['view'], $actions['delete'], $actions['create'], $actions['update']);

        // customize the data provider preparation with the "prepareDataProvider()" method
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function prepareDataProvider()
    {
        // prepare and return a data provider for the "index" action
        $modelClass = $this->modelClass;

        $query = $modelClass::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->andFilterWhere(['!=', 'is_deleted', 1]);

        return $dataProvider;
    }

    public function actionView() {
        $id = Yii::$app->request->get('id');
        $modelClass = $this->modelClass;

        if ($model = $modelClass::find()->where(['and', ['id' => $id], ['!=', 'is_deleted', '1']])->one()) {
            return $model;
        } else {
            throw new \yii\web\NotFoundHttpException('404 Data not Found');
        }
    }

    public function actionDelete() {
        $id = Yii::$app->request->get('id');
        $modelClass = $this->modelClass;

        $model = $modelClass::findOne(['id' => $id]);
        if (!$model) {
            throw new \yii\web\NotFoundHttpException('404 Data not Found');
        }

        $model->is_deleted = 1;
        $model->save();
    }


    public function actionSearch() {
        $modelClass = $this->modelClass;
        $dArrPost = Yii::$app->request->post();

        $model = $modelClass::findOne(['username' => @$dArrPost['username']]);

        $resp = [];
        if (!$model) {
            $resp['code'] = 1;
            $resp['message'] = "Username not found";
        } else {
            if (!$model->validatePassword(@$dArrPost['password'])) {
                $resp['code'] = 2;
                $resp['message'] = "Incorrect password";
            } else {
                $resp['code'] = 0;
                $resp['message'] = $model->id . "|" . $model->role . "|" . $model->username;
            }
        }

        return $resp;
    }

    public function actionCreate() {
        $modelClass = $this->modelClass;
        if ($modelClass::findOne(['username' => Yii::$app->request->bodyParams['username']])) {
            throw new ServerErrorHttpException('User exists.');
        }

        $model = new $modelClass;
        $model->setScenario('create');

        $model->load(Yii::$app->getRequest()->getBodyParams(), '');

        if ($model->validate()) {
            $model->setPassword($model->password);

            if ($model->save()) {
                $response = Yii::$app->getResponse();
                $response->setStatusCode(201);
                $response->getHeaders()->set('Location', Url::toRoute(['view', 'id' => $model->id], true));
            } elseif (!$model->hasErrors()) {
                throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
            }
        } else {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        return $model;
    }

    public function actionUpdate() {
        $id = Yii::$app->request->get('id');
        $modelClass = $this->modelClass;

        $model = $modelClass::findOne(['id' => $id]);
        $dStrPrevPass = $model->password;

        $model->load(Yii::$app->getRequest()->getBodyParams(), '');

        if ($model->password != $dStrPrevPass) {
            $model->setPassword($model->password);
        }

        if ($model->save() === false && !$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
        }

        return $model;
    }
}
