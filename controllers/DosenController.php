<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\ServerErrorHttpException;
use yii\imagine\Image;
use Imagine\Gd;
use Imagine\Image\Box;
use Imagine\Image\BoxInterface;
use app\models\Activity;

class DosenController extends ActiveController {
    public $modelClass = "app\models\Dosen";

    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH', 'POST'],
            'delete' => ['DELETE'],
        ];
    }

    public function actions()
    {
        $actions = parent::actions();

        //hapus agar bisa dioverride
        unset($actions['delete'], $actions['view'], $actions['create'], $actions['update']);

        // customize the data provider preparation with the "prepareDataProvider()" method
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function prepareDataProvider()
    {
        // prepare and return a data provider for the "index" action
        $modelClass = $this->modelClass;

        $query = $modelClass::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->andFilterWhere(['!=', 'is_deleted', 1]);

        return $dataProvider;
    }

    public function actionView() {
        $id = Yii::$app->request->get('id');
        $modelClass = $this->modelClass;

        if ($model = $modelClass::find()->where(['and', ['user_id' => $id], ['!=', 'is_deleted', '1']])->one()) {
            return $model;
        } else {
            throw new \yii\web\NotFoundHttpException('404 Data not Found');
        }
    }

    public function actionDelete() {
        $id = Yii::$app->request->get('id');
        $modelClass = $this->modelClass;

        $model = $modelClass::findOne(['user_id' => $id]);
        if (!$model) {
            throw new \yii\web\NotFoundHttpException('404 Data not Found');
        }

        $model->is_deleted = 1;
        $model->save();
    }

    public function actionCreate() {
        $modelClass = $this->modelClass;
        if ($modelClass::findOne(['user_id' => Yii::$app->request->bodyParams['user_id']])) {
            throw new ServerErrorHttpException('1'); //menandakan bahwa udah ada
        }

        $model = new $modelClass;
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');

        $modelUser = \app\models\User::findOne(['id' => Yii::$app->request->bodyParams['user_id']]);
        $modelUser->load(Yii::$app->getRequest()->getBodyParams(), '');

        $photo = \yii\web\UploadedFile::getInstanceByName('photo');

        if ($model->validate() && $modelUser->validate() && $photo) {
            $trans = Yii::$app->db->beginTransaction();
            $success = true;

            $dir = Yii::getAlias('@webroot').'/images/';
            $filename = time() . rand(1000, 9999) . '.' . $photo->getExtension();

            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }

            $save = Image::getImagine()
                        ->open($photo->tempName)
                        ->thumbnail(new Box(500, 500))
                        ->save($dir.$filename);

            if ($save) {
                $model->photo = $filename;
            } else {
                $success = false;
                $trans->rollback();
            }


            if ($success && $model->save()) {
                //nothing
            } else {
                $success = false;
                $trans->rollback();
            }

            $modelUser->setPassword($modelUser->password);
            if ($success && $modelUser->save()) {
                //nothing
            } else {
                $success = false;
                $trans->rollback();
            }



            if ($success) {
                $trans->commit();
                $response = Yii::$app->getResponse();
                $response->setStatusCode(201);
                $response->getHeaders()->set('Location', Url::toRoute(['view', 'id' => $model->user_id], true));
            } elseif (!$model->hasErrors()) {
                $trans->rollback();
                throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
            }
        } else {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        return $model;
    }

    public function actionUpdate() {
        $modelClass = $this->modelClass;
        $id = Yii::$app->request->get('id');

        if (!$model = $modelClass::findOne(['user_id' => $id])) {
            throw new \yii\web\NotFoundHttpException('404 Data not Found');
        }
        $prevPhoto = $model->photo;

        $model->load(Yii::$app->getRequest()->getBodyParams(), '');

        $modelUser = \app\models\User::findOne(['id' => $id]);

        $prevPass = $modelUser->password;

        $modelUser->load(Yii::$app->getRequest()->getBodyParams(), '');

        $photo = \yii\web\UploadedFile::getInstanceByName('photo');

        if ($model->validate() && $modelUser->validate()) {
            $trans = Yii::$app->db->beginTransaction();
            $success = true;

            if ($photo) {
                $dir = Yii::getAlias('@webroot').'/images/';
                $filename = time() . rand(1000, 9999) . '.' . $photo->getExtension();

                if (!file_exists($dir)) {
                    mkdir($dir, 0777, true);
                }

                $save = Image::getImagine()
                            ->open($photo->tempName)
                            ->thumbnail(new Box(500, 500))
                            ->save($dir.$filename);

                if ($save) {
                    $model->photo = $filename;
                } else {
                    $success = false;
                    $trans->rollback();
                }
            } else {
                $model->photo = $prevPhoto;
            }


            if ($success && $model->save()) {
                //nothing
            } else {
                $success = false;
                $trans->rollback();
            }


            if ($modelUser->password && isset(Yii::$app->request->bodyParams['password'])) {
                $modelUser->setPassword($modelUser->password);
            } else {
                $modelUser->password = $prevPass;
            }

            if ($success && $modelUser->save()) {
                //nothing
            } else {
                $success = false;
                $trans->rollback();
            }


            if ($success) {
                $trans->commit();
                $response = Yii::$app->getResponse();
                $response->setStatusCode(201);
                $response->getHeaders()->set('Location', Url::toRoute(['view', 'id' => $model->user_id], true));
            } elseif (!$model->hasErrors()) {
                $trans->rollback();
                throw new ServerErrorHttpException('Failed to save the object for unknown reason.');
            }
        } else {
            throw new ServerErrorHttpException('Failed to save the object for unknown reason.');
        }

        return $model;
    }

    public function actionActivity($id) {
        $id = Yii::$app->request->get('id');
        $offlineCount = 2*60;
        $limit = 10;

        $model = Activity::find()
                    ->where(['dosen_id' => $id])
                    ->orderBy(['id' => SORT_DESC])
                    ->all();

        $result = [];
        $result['lastSeen'] = -1;

        if ($model) {
            $lastUpdate = $model[0];
            $lastTime = strtotime($lastUpdate->time);
            $now = time();

            if ($now - $lastTime <= $offlineCount) {
                $result['lastSeen'] = 0;
            } else {
                $result['lastSeen'] = (int)(($now - $lastTime) / 60);
            }


            //generate timeline
            $result['timeline'] = [];
            $last = 0;
            $startTime = 0;
            $endTime = 0;
            foreach ($model as $key => $val) {
                if ($key>0) {
                    //var_dump($last-strtotime($val->time));
                    if ($endTime == 0) {
                        $endTime = strtotime($val->time);
                    } else if ($last-strtotime($val->time) > $offlineCount) {
                        $startTime = $last;
                        $temp = [];
                        $temp['startTime'] = date('d M Y H:i:s', $startTime);
                        $temp['endTime'] = date('d M Y H:i:s' ,$endTime);
                        $temp['duration'] = (int)(($endTime-$startTime)/60);
                        $temp['place'] = "";

                        $result['timeline'][] = $temp;
                        $endTime = strtotime($val->time);

                        if (count($result['timeline']) >= $limit) {
                            break;
                        }
                    }
                }
                $last = strtotime($val->time);
            }
        }
        //die();

        return $result;
    }
}
